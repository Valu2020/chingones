<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
//primera ruta de get
Route::get('hola', function(){
echo "Hola estoy en laravel";

});

Route::get('arreglo', function(){

    //Crear un arreglo de estudiantes

    $estudiantes = ["LA" => "Laura" , 
                    "VA" => "Valentina" , 
                    "CA" => "Catalina" , 
                    "KE" => "Kevin" 
                ];
    //var_dump($estudiantes);

    //Recorrer un arreglo
    foreach ( $estudiantes as $indice => $estudiante ) {
        echo "$estudiante tiene indice $indice <hr />";
  
    }


});

Route::get('paises' , function(){
    $paises = [
        "Colombia" => [
            "capital" => "Bogota" ,
            "moneda" => "peso" ,
            "poblacion" => 50.372
            
        ],
        "Ecuador" => [
            "capital" => "Quito" ,
            "moneda" => "Dolar" ,
            "poblacion" => 17.517
        ],
        "Brazil" => [
            "capital" => "Brasilia" ,
            "moneda" => "Real" ,
            "poblacion" => 212.216
        ],
        "Bolivia" => [
            "capital" => "La paz" ,
            "moneda" => "Boliviano" ,
            "poblacion" => 11.633
        ]
    ];
    
   //mostrar una vista para presentar los paises
   // En MVC puedo pasar datos a una vista
   return view('paises')->with("paises" , $paises);

   

});

  //Recorrer la primera dimension del arreglo
   //foreach ($paises as $pais => $infopais) {
       //echo"<h2>$pais</h2>";
       //echo "capital" . $infopais["capital"] . "<br />";
   //echo "moneda" . $infopais["moneda"] . "<br />";
   //echo "poblacion(en millones de habitantes)" . $infopais["poblacion"] . "<br />";
   //echo "<hr />";


